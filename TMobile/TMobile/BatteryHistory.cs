﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TMobile.Adapter;
using TMobile.ViewHolder;

namespace TMobile
{
    [Activity(Label = "BatteryHistory")]
    public class BatteryHistory : Activity
    {
        ListView listView;

        BatteryAdapter batteryAdapter;
        List<BatteryViewHolder> list = new List<BatteryViewHolder>(){
                new BatteryViewHolder(){batterylevel="1" ,timestamp="1"},
                new BatteryViewHolder(){ batterylevel="1" ,timestamp="1"},

            };
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.battery);
            listView = (ListView)FindViewById<ListView>(Resource.Id.listview);
            // listView.Adapter = new ArrayAdapter(this,Resource.Layout.battery_list, list);
            listView.Adapter = new BatteryAdapter(this,list);

            listView.ItemClick += (s, e) => {
                var t = list[e.Position];
                Toast.MakeText(this, t.batterylevel, Android.Widget.ToastLength.Long).Show();
            };
            // Create your application here
        }
    }
}
