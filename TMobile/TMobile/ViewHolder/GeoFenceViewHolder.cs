﻿using System;
using Android.Support.V7.Widget;
using Android.Views;

namespace TMobile.ViewHolder
{
    public class GeoFenceViewHolder
    {
        public string location { get; set; }
        public string timestamp { get; set; }

    }
}
