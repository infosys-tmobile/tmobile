﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using TMobile.ViewHolder;

namespace TMobile.Adapter
{
    public class FotaAdapter : BaseAdapter<FotaViewHolder>
    {


            Activity context;
            List<FotaViewHolder> list;

         public FotaAdapter(Activity _context, List<FotaViewHolder> _list): base()
        {
                this.context = _context;
                this.list = _list;
            }
        public override int Count
        {
            get { return list.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override FotaViewHolder this[int index]
        {
            get { return list[index]; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;

            // re-use an existing view, if one is available
            // otherwise create a new one
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.fota_list, parent, false);

            FotaViewHolder item = this[position];
            view.FindViewById<TextView>(Resource.Id.textView_ver_val).Text = item.version;

            /*using (var imageView = view.FindViewById<ImageView>(Resource.Id.Thumbnail))
            {
                string url = Android.Text.Html.FromHtml(item.thumbnail).ToString();

                //Download and display image
                Koush.UrlImageViewHelper.SetUrlDrawable(imageView,
                    url, Resource.Drawable.Placeholder);
            }*/
            return view;
        

    }
}
}
