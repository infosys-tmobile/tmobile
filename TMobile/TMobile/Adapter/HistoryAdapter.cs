﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using TMobile.ViewHolder;

namespace TMobile.Adapter
{
    public class HistoryAdapter : BaseAdapter<HistoryViewHolder>
    {
        Activity context;
        List<HistoryViewHolder> list;


        public HistoryAdapter(Activity _context, List<HistoryViewHolder> _list)
        : base()
        {
            this.context = _context;
            this.list = _list;
        }
        public override int Count
        {
            get { return list.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override HistoryViewHolder this[int index]
        {
            get { return list[index]; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;

            // re-use an existing view, if one is available
            // otherwise create a new one
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.history_list, parent, false);

            HistoryViewHolder item = this[position];
            view.FindViewById<TextView>(Resource.Id.textview_location_val).Text = item.location;
            view.FindViewById<TextView>(Resource.Id.textview_timestamp_val).Text = item.timestamp;

            /*using (var imageView = view.FindViewById<ImageView>(Resource.Id.Thumbnail))
            {
                string url = Android.Text.Html.FromHtml(item.thumbnail).ToString();

                //Download and display image
                Koush.UrlImageViewHelper.SetUrlDrawable(imageView,
                    url, Resource.Drawable.Placeholder);
            }*/
            return view;
        }

    }
}
