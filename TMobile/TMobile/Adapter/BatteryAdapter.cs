﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using TMobile.ViewHolder;

namespace TMobile.Adapter
{
    public class BatteryAdapter : BaseAdapter<BatteryViewHolder>
    {

            Activity context;
            List<BatteryViewHolder> list;

            public BatteryAdapter(Activity _context, List<BatteryViewHolder> _list): base()
            {
                this.context = _context;
                this.list = _list;
            }
        public override int Count
        {
            get { return list.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override BatteryViewHolder this[int index]
        {
            get { return list[index]; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;

            // re-use an existing view, if one is available
            // otherwise create a new one
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.battery_list, parent, false);

            BatteryViewHolder item = this[position];
            view.FindViewById<TextView>(Resource.Id.textView_time_val).Text = item.timestamp;
            view.FindViewById<TextView>(Resource.Id.textView_battery_val).Text = item.batterylevel;

            
            return view;


        


    }
    }
}
