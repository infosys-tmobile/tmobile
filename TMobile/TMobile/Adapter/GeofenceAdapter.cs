﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using TMobile.ViewHolder;

namespace TMobile.Adapter
{
    public class GeofenceAdapter : BaseAdapter<GeoFenceViewHolder>
    {
        Activity context;
        List<GeoFenceViewHolder> list;

        public GeofenceAdapter(Activity _context, List<GeoFenceViewHolder> _list) : base()
        {
            this.context = _context;
            this.list = _list;
        }
        public override int Count
        {
            get { return list.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override GeoFenceViewHolder this[int index]
        {
            get { return list[index]; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;

            // re-use an existing view, if one is available
            // otherwise create a new one
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.geofence_list, parent, false);

            GeoFenceViewHolder item = this[position];
            view.FindViewById<TextView>(Resource.Id.textView_location_val).Text = item.location;
            view.FindViewById<TextView>(Resource.Id.textView_time_val).Text = item.timestamp;

            /*using (var imageView = view.FindViewById<ImageView>(Resource.Id.Thumbnail))
            {
                string url = Android.Text.Html.FromHtml(item.thumbnail).ToString();

                //Download and display image
                Koush.UrlImageViewHelper.SetUrlDrawable(imageView,
                    url, Resource.Drawable.Placeholder);
            }*/
            return view;


        }
    }
    }
