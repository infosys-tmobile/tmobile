﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using TMobile.Adapter;
using TMobile.ViewHolder;

namespace TMobile.Fragments
{
    public class History : Fragment
    {
        ListView listView;
        HistoryAdapter adapter;
        private ListView lv;
        List<HistoryViewHolder> list = new List<HistoryViewHolder>(){
                new HistoryViewHolder(){ location="30.73315,76.779419", timestamp="11 Nov 2018,1.45"},
                new HistoryViewHolder(){ location="30.73315,76.779419", timestamp="11 Nov 2018,1.45"},

            };
        public override void OnCreate(Bundle savedInstanceState)
        {
           // List<HistoryViewHolder> listData = new List()<>;
            base.OnCreate(savedInstanceState);



            // Create your fragment here
        }
        public static History NewInstance()
        {
            History fragment = new History();
            return fragment;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.fragment_history, container, false);
            Button button1 = v.FindViewById<Button>(Resource.Id.button_battery_history);
            button1.Click += Button_Click;
            lv = v.FindViewById<ListView>(Resource.Id.listview_history);

            //ADAPTER
            adapter = new HistoryAdapter(this.Activity, list);
            lv.Adapter = adapter;

            //ITEM CLICKS
            lv.ItemClick += lv_ItemClick;

            return v;


            // Use this to return your custom view for this Fragment

            //return base.OnCreateView(inflater, container, savedInstanceState);
        }
        private void Button_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(Activity, typeof(BatteryHistory));
            StartActivity(intent);

        }
        void lv_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Toast.MakeText(this.Activity, "hi", ToastLength.Short).Show();
        }

    }
}