﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using TMobile.Adapter;
using TMobile.ViewHolder;

namespace TMobile.Fragments
{
    public class Fota : Android.Support.V4.App.Fragment
    {
        ListView listView;
        FotaAdapter adapter;
        private ListView lv;
        List<FotaViewHolder> list = new List<FotaViewHolder>(){
                new FotaViewHolder(){ version="1.1"},
                new FotaViewHolder(){ version="1.2"},

            };
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        public static Fota NewInstance()
        {
            Fota fragment = new Fota();
            return fragment;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.fragment_fota, container, false);

            lv = v.FindViewById<ListView>(Resource.Id.listview_fota);

            //ADAPTER
            adapter = new FotaAdapter(this.Activity, list);
            lv.Adapter = adapter;

            //ITEM CLICKS
            lv.ItemClick += lv_ItemClick;

            return v;

        }
        void lv_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Toast.MakeText(this.Activity, "hi", ToastLength.Short).Show();
        }
    }
}