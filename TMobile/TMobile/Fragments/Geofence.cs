﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using TMobile.Adapter;
using TMobile.ViewHolder;

namespace TMobile.Fragments
{
    public class Geofence : Fragment
    {
        ListView listView;
        GeofenceAdapter adapter;
        private ListView lv;
        List<GeoFenceViewHolder> list = new List<GeoFenceViewHolder>(){
                new GeoFenceViewHolder(){ location = "Lat,Lang,Radius", timestamp = "13 Nov 2018,1.45" },
                new GeoFenceViewHolder(){ location = "Lat,Lang,Radius", timestamp = "14 Nov 2018,1.45" },

            };
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        public static Geofence NewInstance()
        {
            Geofence fragment = new Geofence();
            return fragment;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.fragment_geofence, container, false);

            lv = v.FindViewById<ListView>(Resource.Id.listView_geofence);

            //ADAPTER
            adapter = new GeofenceAdapter(this.Activity, list);
            lv.Adapter = adapter;

            //ITEM CLICKS
            lv.ItemClick += lv_ItemClick;

            return v;

        }
        void lv_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Toast.MakeText(this.Activity, "hi", ToastLength.Short).Show();
        }
    }
}